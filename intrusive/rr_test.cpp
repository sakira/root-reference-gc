#include "rrobject.hpp"
#include "rrptr.hpp"
#include "bilist_test.hpp"
#include "rrvector_test.hpp"
#include <string>
#include <iostream>

using namespace std;

#ifdef DEBUG_PRINT
static void display(const RRObject& obj) {
  cout << &obj << ","
       << " succ_: " << &(obj.succ_)
       << " pred_: " << &(obj.pred_) << endl;
}
#endif

class B : public RRObject {
public:
  int b;
  
  RRPtr<B> child;

  B(int b_) : b(b_) {
    cout << "created:" << b << endl;
    child.setOwner(this);
  }
  ~B(void) {}
  virtual void destroy(void) {
    cout << "destroyed:" << b << endl;
    RRObject::destroy();
  }

  #ifdef DEBUG_PRINT
  void display(void) {
    cout << "name: b" << b << endl;
    cout << "proot_: " << proot_ << endl;
    cout << "succ_ ";
    for (auto& n : succ_) cout << n.first << ":" << n.second << " ";
    cout << "\npred_ ";
    for (auto& n : pred_) cout << n.first << ":" << n.second << " ";
    cout << endl;
  }
  #endif
  
};

int main(void) {
  typedef RRPtr<B> BPtr;

  #ifdef DEBUG_PRINT
  cout << "seed::";
  display(bilist<int>::seed_);
  cout << "delete::";
  display(bilist<int>::delete_mark_);
  #endif
  
  if (1) {
    if (1) {
      cout << "**** test for reference counting" << endl;
      BPtr b1(new B(1));
      {
        BPtr b2(new B(2));
        {
          BPtr b3(new B(3));
          b1 = b3;
        }
      }
    }
    
    if (1) {
      cout << "**** very simple child test" << endl;
      BPtr b10(new B(10));
      BPtr b11(new B(11));
      b10->child = b11;
    }
    
    if (1) {
      cout << "**** very simple child test 2" << endl;
      BPtr b20(new B(20));
      b20->child = RRPtr<B>(new B(21));
      b20 = RRPtr<B>(NULL);
    }
    if (1) {
      cout << "**** very simple child test 2a" << endl;
      BPtr b22(new B(22));
      b22->child = new B(23);
      b22 = NULL;
    }

    if (1) {
      cout << "**** cyclic-collection test" << endl;
      BPtr b30(new B(30));
#ifdef DEBUG_PRINT
      cout << "b30::";
      display(*b30.pointee_);
#endif

      {
        BPtr b31(new B(31));
#ifdef DEBUG_PRINT
	cout << "b31::";
	display(*b31.pointee_);
#endif
        b30->child = b31;
      }
      b30->child->child = b30;
    }

    if (1) {
      cout << "**** assignment test" << endl;
      BPtr b40(new B(40));
      BPtr b41 = b40;
    }
  }

  if (1) {
    bilist_test();
  }

  if (1) {
    rrvector_test();
  }
  
  cout << "**** end of test" << endl;
  
  return 0;
}
