#include "rrobject.hpp"
#include <map>
#include <set>
#include <utility>
#include <iostream>
#include <cassert>

using namespace std;

//////////////////////////////////////// RRIds

#ifdef DEBUG_PRINT
void RRIds::display(void) const {
  cout << "RRIds:" << this << ": ";
  for (const auto& rrid : *this) 
    cout << rrid.first << "->" << rrid.second << ", ";
  cout << endl;
}
#endif

size_t RRIds::insert(RRObject* const nid) {
  if (find(nid) == end()) {
    (*this)[nid] = 1;
  } else {
    (*this)[nid] ++;
  }
#ifdef DEBUG_PRINT
  cout << "insert() " << (*this)[nid] << " " << this << " " << nid << endl;
  display();
#endif 
  return (*this)[nid];   
}

size_t RRIds::remove(RRObject* const oid) {
  assert(find(oid) != end());
  assert((*this)[oid] > 0);
  (*this)[oid] --;
#ifdef DEBUG_PRINT
  cout << "remove() " << (*this)[oid] << " " << this << " " << oid << endl;
  display();
#endif
  if (!(*this)[oid]) {
     erase(oid);
     return 0;
  } else {
     return (*this)[oid];
  }
}

//////////////////////////////////////// RRObject

RRObject RRObject::seed_;
RRObject RRObject::delete_mark_;
RRObject RRObject::pre_delete_mark_;

RRObject::RRObject(void) {}

RRObject::RRObject(const RRObject& rhs){}

RRObject::~RRObject(void) {}

RRObject& RRObject::operator=(const RRObject& rhs) {
  return *this;
}

void RRObject::createNode(void) {
  // this is a root
#ifdef DEBUG_PRINT
  cout << "createNode()" << endl;
#endif
  seed_.succ_.insert(this);
  setUpRoot();
}

void RRObject::setUpRoot(void) {
#ifdef DEBUG_PRINT
  cout << "setUpRoot()" << endl;
#endif
  pred_.clear();
  pred_.insert(&seed_);
  proot_ = this;
}

void RRObject::attachEdge(RRObject& node) {
#ifdef DEBUG_PRINT
  cout << "attachEdge():" << endl;
#endif
  succ_.insert(&node);
  nailKnot(node);
}

bool RRObject::initialized(void) const {
  return (proot_ != nullptr);
}

bool RRObject::isRoot(void) const {
  return (pred_.find(&seed_) != pred_.end());
}

bool RRObject::isDying(void) const {
  return dying_;
}

void RRObject::nailKnot(RRObject& node) {
#ifdef DEBUG_PRINT
  cout << "nailKnot()" << endl;
#endif
  node.pred_.insert(this);
}

set<RRObject*> RRObject::detachEdge(RRObject& node) {
#ifdef DEBUG_PRINT
  cout << "detachEdge():" << this << " " << &node << endl;
#endif
  succ_.remove(&node);
  const auto deleting = nailUnused(node);
  
#if DEBUG_PRINT
  cout << "deleting: ";
  for (const auto& n : deleting)
    cout << n << ",";
  cout << endl;
#endif
  
  trashGarbage(deleting);
  return deleting;
}

set<RRObject*> RRObject::nailUnused(RRObject& node) {
#ifdef DEBUG_PRINT
  cout << "nailUnused()" << endl;
#endif
  const size_t mult = node.pred_.remove(this);
  if (mult > 0) return set<RRObject*>();

  if (this == &seed_) {
    // this is seminal
    node.refresh_proot(&node);
    set<RRObject*> delete_nodes;
    node.collect_deleting_nodes(delete_nodes);
    return delete_nodes;
  } else {
    // this is root
    node.replace_proot(proot_, &node);
    return set<RRObject*>();
  }
}

void RRObject::trashGarbage(const set<RRObject*> delete_nodes) {
#ifdef DEBUG_PRINT
  cout << "trashGarbage()" << endl;
#endif
  for (auto& node : delete_nodes) {
    for (auto& succ : node->succ_)
      succ.first->pred_.erase(node);
    for (auto& pred : node->pred_)
      pred.first->succ_.erase(node);
    node->succ_.clear();
    node->pred_.clear();
    node->dying_ = true;
  }
  for (auto& node : delete_nodes)
    node->destroy();
}

// private methods

void RRObject::replace_proot(const RRObject* const from_root,
			     RRObject* const to_root) {
#ifdef DEBUG_PRINT
  cout << "replace_proot():" << this <<
    " now:" << proot_ << " from:" << from_root <<
    " to:" << to_root << endl;
#endif
  if (proot_ != from_root) return;
  proot_ = to_root;
  for (auto& child : succ_)
    child.first->replace_proot(from_root, to_root);
}

RRObject* RRObject::refreshed_proot_from(const RRObject* const from_root) {
  if (isRoot() && this != from_root)
    return this;

  bool all_delete_mark = true;
  for (const auto& p_pair : pred_) {
    const auto& parent = p_pair.first;
    if (parent->isRoot() && parent != from_root) {
      return parent;
    }
    if (parent != &seed_) {
      const auto& pproot = parent->proot_;
      if (pproot != from_root && 
          pproot != &delete_mark_ &&
          pproot != &pre_delete_mark_)
        return pproot;
      if (pproot != &delete_mark_)
        all_delete_mark = false;
    }
  }
  if (all_delete_mark)
    return &delete_mark_;
  else
    return nullptr;
}

void RRObject::refresh_proot(RRObject* const from_root) {
#ifdef DEBUG_PRINT
  cout << "refresh_proot():" << this <<
    " now:" << proot_ <<
    " from:" << from_root << endl;
  pred_.display();
#endif
  if (proot_ != from_root && 
      proot_ != &pre_delete_mark_) return;
  const auto& new_proot = refreshed_proot_from(from_root);
#ifdef DEBUG_PRINT
  cout << "refreshed proot:" << new_proot << endl;
#endif
  if (new_proot == nullptr && 
      proot_ == &pre_delete_mark_) return;

  if (new_proot != nullptr) 
    proot_ = new_proot;
  else
    proot_ = &pre_delete_mark_;

  for (auto& child : succ_)
    child.first->refresh_proot(from_root);
}

void RRObject::collect_deleting_nodes(set<RRObject*> &delete_nodes) {
#ifdef DEBUG_PRINT
  cout << "collect_deleting_nodes():" << this << " proot_:" << proot_ << endl;
#endif
  if (proot_ == &pre_delete_mark_ || proot_ == &delete_mark_) {
    delete_nodes.insert(this);
    for (const auto& child : succ_) {
      const auto& c = child.first;
      if (delete_nodes.find(c) == delete_nodes.end())
	c->collect_deleting_nodes(delete_nodes);
    }
  }
}
