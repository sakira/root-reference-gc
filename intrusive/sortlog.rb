#!/usr/bin/ruby

destroyed_lines = []
while line = gets do 
  if /destroyed:/ =~ line then
    destroyed_lines << line
  else
    if destroyed_lines.length > 0 then
      destroyed_lines.sort.each {|l| print l}
      destroyed_lines.clear
    end
    print line
  end
end
print destroyed_lines.sort.each {|l| print l}
