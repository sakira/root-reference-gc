#include "bilist_test.hpp"
#include "rrptr.hpp"
#include <iostream>

typedef RRPtr<bilist<int> > intlist;
int curr_counter_ = 0;
using namespace std;

#ifdef DEBUG_PRINT
static void display(const RRObject& obj) {
  cout << &obj << ","
       << " succ_: " << &(obj.succ_)
       << " pred_: " << &(obj.pred_) << endl;
}
#endif

int bilist_test(void) {
  cout << "******* bidirectional list test" << endl;

#ifdef DEBUG_PRINT
  cout << "seed::";
  display(bilist<int>::seed_);
  cout << "delete::";
  display(bilist<int>::delete_mark_);
#endif
  
  if (1) {
    cout << "**** pre test" << endl;
    intlist head(new bilist<int>(10));
#ifdef DEBUG_PRINT
    cout << "b10::";
    display(*head.pointee_);
#endif
    intlist curr = head;
    intlist new_node(new bilist<int>(20));
#ifdef DEBUG_PRINT
    cout << "b20::";
    display(*new_node.pointee_);
#endif
    
    new_node->insertAfter(curr);
    curr = new_node;

    new_node = new bilist<int>(30);
#ifdef DEBUG_PRINT
    cout << "b30::";
    display(*new_node.pointee_);
#endif
    new_node->insertAfter(curr);
    curr = new_node;

    new_node = new bilist<int>(40);
#ifdef DEBUG_PRINT
    cout << "b40::";
    display(*new_node.pointee_);
#endif
    new_node->insertAfter(curr);
    curr = new_node;
  }

  if (1) {
    cout << "**** pretest 2 (insert)" << endl;
    intlist head(new bilist<int>(10));
#ifdef DEBUG_PRINT
    cout << "b10::";
    display(*head.pointee_);
#endif
    {
      intlist curr = head;
      for (int i = 2; i < 4; i++) {
	intlist node(new bilist<int>(i * 10));
#ifdef DEBUG_PRINT
	cout << "b" << (i * 10) << "::";
	display(*node.pointee_);
#endif
	node->insertAfter(curr);
	curr = curr->nextNode;
      }
    }
    {
      intlist b25 = new bilist<int>(25);
#ifdef DEBUG_PRINT
      cout << "b25::";
      display(*b25.pointee_);
      cout << "## b25->insertAfter(head->nextNode);" << endl;
#endif
      b25->insertAfter(head->nextNode);
    }
    
    intlist node = head;
    for (int i = 0; i < 20 && node; i ++, node = node->nextNode)
      cout << "result: " << i << ":" << node->value() << endl;
  }
      
  if (1) {
    cout << "**** pretest 3 (delete)" << endl;
    intlist head(new bilist<int>(10));
#ifdef DEBUG_PRINT
    cout << "b10::";
    display(*head.pointee_);
#endif
    {
      intlist curr = head;
      for (int i = 2; i < 4; i++) {
	intlist node(new bilist<int>(i * 10));
#ifdef DEBUG_PRINT
	cout << "b" << (i * 10) << "::";
	display(*node.pointee_);
#endif
	node->insertAfter(curr);
	curr = curr->nextNode;
      }
    }
    head->nextNode->remove();

    intlist node = head;
    for (int i = 0; i < 20 && node; i ++, node = node->nextNode) {
      cout << "result: " << i << ":" << node->value() << endl;
    }
  }

  
  if (1) {
    cout << "**** linear list" << endl;
    intlist head(new bilist<int>(10));
#ifdef DEBUG_PRINT
    cout << "b10::";
    display(*head.pointee_);
#endif
    {
      intlist curr = head;
      for (int i = 2; i < 10; i++) {
	intlist node(new bilist<int>(i * 10));
#ifdef DEBUG_PRINT
	cout << "b" << (i * 10) << "::";
	display(*node.pointee_);
	cout << "## node->insertAfter(curr);" << endl;
#endif
	node->insertAfter(curr);
	curr = curr->nextNode;
      }
    }
    {
    intlist b35 = new bilist<int>(35);
#ifdef DEBUG_PRINT
    cout << "b35::";
    display(*b35.pointee_);
#endif
    b35->insertAfter(head->nextNode->nextNode);
    }
    // removing b40
    head->nextNode->nextNode->nextNode->nextNode->remove();
    
    intlist node = head;
    for (int i = 0; i < 20 && node; i ++, node = node->nextNode)
      cout << "result: " << i << ":" << node->value() << endl;
  }

  if (1) {
    cout << "**** cyclic list" << endl;
    intlist head(new bilist<int>(10));
#ifdef DEBUG_PRINT
    cout << "b10::";
    display(*head.pointee_);
#endif
    {
      intlist curr = head;
      for (int i = 2; i < 10; i++) {
	intlist node(new bilist<int>(i * 10));
#ifdef DEBUG_PRINT
	cout << "b" << (i * 10) << "::";
	display(*node.pointee_);
#endif
	node->insertAfter(curr);
	curr = curr->nextNode;
      }
      curr->nextNode = head;
    }

    intlist(new bilist<int>(35))->insertAfter(head->nextNode->nextNode);
    head->nextNode->nextNode->nextNode->nextNode->remove();

    intlist node = head;
    for (int i = 0; i < 20 && node; i ++, node = node->nextNode)
      cout << "result: " << i << ":" << node->value() << endl;
  }

  return 0;
}
