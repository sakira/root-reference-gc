#!/usr/bin/ruby

lines = readlines
variables = {}
lines.each do |l|
  if /(.+)::([x0-9a-f]+), succ_: ([x0-9a-f]+) pred_: ([x0-9a-f]+)/ =~ l then
    varname = $1.to_s
    variables[$2.to_s] = varname
    variables[$3.to_s] = varname + ".succ"
    variables[$4.to_s] = varname + ".pred"
  end
end
lines.each do |l|
  variables.each do |k,v|
    l.gsub!(k, v)
  end
  print l
end
