#ifndef RROjbect_header
#define RROjbect_header 1

#include <map>
#include <set>
#include "rrptr_decl.hpp"

using namespace std;

class RRObject;

class RRIds : public map<RRObject*, size_t> {
public:
  size_t insert(RRObject* const nid);
  size_t remove(RRObject* const oid);

#ifdef DEBUG_PRINT
  void display(void) const;
#endif
};

class RRObject {
  // base class for root-referencing objects
    
public:
  void createNode(void);
  void attachEdge(RRObject& node);
  set<RRObject*> detachEdge(RRObject& node);
  bool initialized(void) const;
  bool isRoot(void) const;
  bool isDying(void) const;

  void attach(void) {seed_.attachEdge(*this);}
  set<RRObject*> detach(void) {return seed_.detachEdge(*this);}

protected:
  RRObject(void);
  RRObject(const RRObject& rhs);
  virtual ~RRObject(void);
  
  virtual RRObject& operator=(const RRObject& rhs);
  virtual void destroy(void) {delete this;}
  
#ifdef DEBUG_PRINT
public:
#else
private:
#endif
  void setUpRoot(void);
  void nailKnot(RRObject& node);
  set<RRObject*> nailUnused(RRObject& node);
  void trashGarbage(set<RRObject*> delete_nodes);

  void replace_proot(const RRObject* const from_root,
		     RRObject* const to_root);
  RRObject* refreshed_proot_from(const RRObject* const from_root);
  void refresh_proot(RRObject* const from_root);
  void collect_deleting_nodes(set<RRObject*> &delete_nodes);
  static RRObject seed_, delete_mark_, pre_delete_mark_;

  RRIds succ_, pred_;
  RRObject* proot_ = nullptr;
  bool dying_ = false;
};

#endif // RROjbect_header 
