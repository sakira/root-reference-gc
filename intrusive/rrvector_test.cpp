#include "rrvector_test.hpp"
#include "rrptr.hpp"
#include <iostream>

using namespace std;

class rrint : public rrvector<rrint> {
public:
  int value_;

  rrint(int val) : value_(val) {
    cout << "rrint created:" << val << endl;
  }
  ~rrint(void) {}
  virtual void destroy(void) {
    cout << "rrint destroyed:" << value_ << endl;
    rrvector::destroy();
  }
};

typedef RRPtr<rrint> intvec;

#ifdef DEBUG_PRINT
static void display(const RRObject& obj) {
  cout << &obj << ","
       << " succ_: " << &(obj.succ_)
       << " pred_: " << &(obj.pred_) << endl;
}
#endif

int rrvector_test(void) {
  cout << "**************** variable length array test" << endl;

#ifdef DEBUG_PRINT
  cout << "seed::";
  display(rrvector<rrint>::seed_);
  cout << "delete::";
  display(rrvector<rrint>::delete_mark_);
#endif

  if (1) {
    cout << "**** simple array test" << endl;
    intvec vec(new rrint(-1));
#ifdef DEBUG_PRINT
    cout << "b_1::";
    display(*vec.pointee_);
#endif
    vec->resize(3);
    for (int i = 0; i < 3; i ++) {
      vec->at(i) = new rrint(i * 10 + 1);
#ifdef DEBUG_PRINT
      cout << "b" << (i * 10 + 1) << "::";
      display(*vec->at(i).pointee_);
#endif
    }
  }
  
  if (1) {
    cout << "**** directional 2 depth tree test" << endl;
    intvec vec(new rrint(-1));
    vec->resize(5);
    for (int i = 0; i < 5; i ++) {
      vec->at(i) = new rrint((i + 1) * 10);
#ifdef DEBUG_PRINT
      cout << "b" << (i + 1) * 10 << "::";
      display(*(vec->at(i).pointee_));
#endif
      vec->at(i)->resize(i);
      for (int j = 0; j < i; j ++) {
	vec->at(i)->at(j) = new rrint((i + 1) * 10 + (j + 1));
      }
    }
    cout << "result ";
    for (int i = 0; i < 10; i ++)
      for (int j = 0; j < 10; j ++)
	if (i < vec->size() &&
	    j < vec->at(i)->size())
	  cout << " " << i << "," << j << ":" << vec->at(i)->at(j)->value_;
    cout << endl;
  }
  
  if (1) {
    cout << "**** resize test of depth 2 directional tree" << endl;
    intvec vec(new rrint(-1));
#ifdef DEBUG_PRINT
    cout << "b_1::";
    display(*vec.pointee_);
#endif
    vec->resize(5);
    for (int i = 0; i < 5; i ++) {
      vec->at(i) = new rrint((i + 1) * 10);
#ifdef DEBUG_PRINT
      cout << "b" << (i + 1) * 10 << "::";
      display(*(vec->at(i).pointee_));
#endif
      vec->at(i)->resize(i);
      for (int j = 0; j < i; j ++) {
	vec->at(i)->at(j) = new rrint((i + 1) * 10 + (j + 1));
#ifdef DEBUG_PRINT
	cout << "b" << (i + 1) * 10 + (j + 1) << "::";
	display(*(vec->at(i)->at(j).pointee_));
#endif
      }
    }
    vec->resize(3);
    
    cout << "result ";
    for (int i = 0; i < 10; i ++)
      for (int j = 0; j < 10; j ++)
	if (i < vec->size() &&
	    j < vec->at(i)->size())
	  cout << " " << i << "," << j << ":" << vec->at(i)->at(j)->value_;
    cout << endl;
  }

  if (1) {
    cout << "**** re-resize test" << endl;
    intvec vec(new rrint(-1));
#ifdef DEBUG_PRINT
    cout << "b_1::";
    display(*vec.pointee_);
#endif
    vec->resize(5);
    for (int i = 0; i < 5; i ++) {
      vec->at(i) = new rrint((i + 1) * 10);
#ifdef DEBUG_PRINT
      cout << "b" << (i + 1) * 10 << "::";
      display(*(vec->at(i).pointee_));
#endif
    }
    vec->resize(6);
    vec->at(5) = new rrint(60);
#ifdef DEBUG_PRINT
    cout << "b60::";
    display(*(vec->at(5).pointee_));
#endif
  }

  if (1) {
    cout << "**** cyclic 2 depth tree test" << endl;
    intvec vec(new rrint(-1));
#ifdef DEBUG_PRINT
    cout << "b_1::";
    display(*vec.pointee_);
#endif
    vec->resize(5);
    for (int i = 0; i < 5; i ++) {
      vec->at(i) = new rrint((i + 1) * 10);
#ifdef DEBUG_PRINT
      cout << "b" << (i + 1) * 10 << "::";
      display(*(vec->at(i).pointee_));
#endif
      vec->at(i)->resize(i);
      for (int j = 0; j < i; j ++) {
	vec->at(i)->at(j) = new rrint((i + 1) * 10 + (j + 1));
#ifdef DEBUG_PRINT
	cout << "b" << (i + 1) * 10 + (j + 1) << "::";
	display(*(vec->at(i)->at(j).pointee_));
#endif
      }
    }
#ifdef DEBUG_PRINT
    cout << "## vec->resize(6);" << endl;
#endif
    vec->resize(6);
#ifdef DEBUG_PRINT
    cout << "## vec->at(5) = vec;" << endl;
#endif
    vec->at(5) = vec;

    cout << "result ";
    for (int i = 0; i < 10; i ++)
      for (int j = 0; j < 10; j ++)
	if (i < vec->size() &&
	    j < vec->at(i)->size())
	  cout << " " << i << "," << j << ":" << vec->at(i)->at(j)->value_;
    cout << endl;
    
    for (int i = 0; i < 10; i ++)
      for (int j = 0; j < 10; j ++)
	for (int k = 0; k < 10; k ++)
	  if (i < vec->size() &&
	      j < vec->at(i)->size() &&
	      k < vec->at(i)->at(j)->size())
	    cout << " " << i << "," << j << "," << k << ":" << vec->at(i)->at(j)->at(k)->value_;
    cout << endl;
  }  
  
  return 0;
}
