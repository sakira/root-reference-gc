#ifndef RRVector_header
#define RRVector_header 1

#include "rrptr.hpp"
#include <vector>
#include <iostream>
#include <cassert>

using namespace std;

template<class T>
class rrvector : public RRObject {
  typedef unsigned int uint;

public:
  RRPtr<T> *ptr_array_ = nullptr;
  uint capacity_ = 0;
  uint size_ = 0;
  
  rrvector(void) {
#ifdef DEBUG_PRINT
    cout << "rrvector constructor" << endl;
#endif
    ptr_array_ = new RRPtr<T>[0];
    capacity_ = size_ = 0;
    reserve(0);
  }
  
  ~rrvector(void) {
#ifdef DEBUG_PRINT
    cout << "rrvector~()" << endl;
#endif
    delete[] ptr_array_;
  }

  virtual void destroy(void) {
#ifdef DEBUG_PRINT
    cout << "rrvector destroyed" << endl;
#endif
    RRObject::destroy();
  }

  void reserve(const uint cap) {
    if (cap < capacity_) return;
    RRPtr<T> *new_array = new RRPtr<T>[cap];
    for (uint i = 0; i < cap; i ++)
      new_array[i].setOwner(this);
    for (uint i = 0; i < size_; i ++)
      new_array[i] = ptr_array_[i];
    delete[] ptr_array_;
    ptr_array_ = new_array;
    capacity_ = cap;
  }

  void resize(const uint size, RRPtr<T> val_ptr) {
    resize(size, &(*val_ptr));
  }

  void resize(const uint size, T* value = nullptr) {
    reserve(size);
    for (uint i = size; i < size_; i ++)
      ptr_array_[i] = nullptr;
    for (uint i = size_; i < size; i ++)
      ptr_array_[i] = value;
    size_ = size;

#ifdef DEBUG_PRINT
    cout << "rrvector::resize() size:" << size_ << " capacity:" << capacity_ << endl;
#endif
  }

  uint size(void) const {
    return size_;
  }

  void clear(void) {
    resize(0, nullptr);
  }

  RRPtr<T>& at(const uint pos) {
    assert(pos >= 0 && pos < size_);
    return ptr_array_[pos];
  }

  RRPtr<T>& operator[](const uint pos) {
#ifdef DEBUG_PRINT
    assert(pos >= 0 && pos < size_);
#endif
    return ptr_array_[pos];
  }
};

#endif // RRVector_header
