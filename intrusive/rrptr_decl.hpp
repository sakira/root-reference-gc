#ifndef RRPtr_decl_header
#define RRPtr_decl_header 1

class RRObject;

template<class T>
class RRPtr {
  // template class for smart pointers to T objects;
  // T must support the RRObject

public:
  RRPtr(T* realPtr = 0, RRObject* owner = 0);
  RRPtr(T* realPtr, RRPtr<RRObject> owner);
  RRPtr(const RRPtr& rhs, RRObject* owner = 0);
  RRPtr(const RRPtr& rhs, RRPtr<RRObject> owner);
  virtual ~RRPtr(void);
  void setOwner(RRObject* owner);
  
  RRPtr& operator=(const RRPtr& rhs);
  RRPtr& operator=(T* rhs);
  T* operator->(void) const;
  T& operator*(void) const;
  operator bool(void) const;

#ifdef DEBUG_PRINT
public: // for debug
#else
private:
#endif

  T *pointee_ = nullptr;
  RRObject *owner_ = nullptr;
  void init(void);

#ifdef DEBUG_PRINT
  int counter_; // for debug
#endif
};

#ifdef DEBUG_PRINT
extern int curr_counter_; // for debug
#endif

#endif // RRPtr_decl_header
