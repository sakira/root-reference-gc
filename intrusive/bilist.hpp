#ifndef BiList_header
#define BiList_header 1

#include "rrptr.hpp"
#include <iostream>

using namespace std;

template<class T>
class bilist : public RRObject {
  T value_;

public:
  RRPtr<bilist> prevNode, nextNode;

  bilist(T v = 0) : value_(v) {
    cout << "bilist created:" << value_ << endl;
    prevNode.setOwner(this);
    nextNode.setOwner(this);
  }

  virtual ~bilist(void) {
    #ifdef DEBUG_PRINT
    cout << "bilist destructed:" << value_ << endl;
    #endif
  }
  
  virtual void destroy(void) {
    cout << "bilist destroyed:" << value_ << endl;
    RRObject::destroy();
  }

  T& value(void) {return value_;}

  // insert this before elmt
  void insertBefore(const RRPtr<bilist>& elmt) {
    this->prevNode = elmt->prevNode;
    this->nextNode = elmt;
    if (elmt->prevNode)
      elmt->prevNode->nextNode = this;
    elmt->prevNode = this;
  }

  // insert this after elmt
  void insertAfter(const RRPtr<bilist>& elmt) {
    this->nextNode = elmt->nextNode;
    this->prevNode = elmt;
    if (elmt->nextNode)
      elmt->nextNode->prevNode = this;
    elmt->nextNode = this;
  }

  // for debug
  static void insert_after(const RRPtr<bilist>& node,
                   const RRPtr<bilist>& elmt) {
    node->nextNode = elmt->nextNode;
    node->prevNode = elmt;
    if (elmt->nextNode)
      elmt->nextNode->prevNode = node;
    elmt->nextNode = node;
  }

  void remove(void) {
    if (this->nextNode)
      this->nextNode->prevNode = this->prevNode;
    if (this->prevNode)
      this->prevNode->nextNode = this->nextNode;
  }
};

#endif // BiList_header
