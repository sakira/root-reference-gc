#ifndef RRPtr_header
#define RRPtr_header 1

#include "rrptr_decl.hpp"
#include "rrobject.hpp"
#include <iostream>

using namespace std;

template<class T>
void RRPtr<T>::init(void) {
  if (!pointee_) return;

  if (pointee_->initialized())
    pointee_->attach();
  else
    pointee_->createNode();

  if (owner_) {
    owner_->attachEdge(*pointee_);
    pointee_->detach();
  }
}

template<class T>
RRPtr<T>::RRPtr(T* realPtr, RRObject* owner) 
  : pointee_(realPtr), owner_(owner) {
#ifdef DEBUG_PRINT
  counter_ = ++ curr_counter_;
  cout << "RRPtr:" << pointee_ << " PP #" << counter_ << endl;
  cout << "this:" << this << endl;
#endif
  init();
}

template<class T>
RRPtr<T>::RRPtr(T* realPtr, RRPtr<RRObject> owner)
  : pointee_(realPtr), owner_(owner.pointee_) {
#ifdef DEBUG_PRINT
  counter_ = ++ curr_counter_;
  cout << "RRPtr:" << pointee_ << " PO #" << counter_ << endl;
  cout << "this:" << this << endl;
#endif
  init();
}

template<class T>
RRPtr<T>::RRPtr(const RRPtr& rhs, RRObject* owner) :
  pointee_(rhs.pointee_), owner_(owner) {
#ifdef DEBUG_PRINT
  counter_ = ++ curr_counter_;
  cout << "RRPtr:" << pointee_ << " OP #" << counter_ << endl;
  cout << "this:" << this << endl;
#endif
  if (pointee_) {
    if (owner_)
      owner_->attachEdge(*pointee_);
    else
      pointee_->attach();
  }
}

template<class T>
RRPtr<T>::RRPtr(const RRPtr& rhs, RRPtr<RRObject> owner) :
  pointee_(rhs.pointee_), owner_(owner.pointee_) {
#ifdef DEBUG_PRINT
  counter_ = ++ curr_counter_;
  cout << "RRPtr:" << pointee_ << " OO #" << counter_ << endl;
  cout << "this:" << this << endl;
#endif
  init();
}

template<class T>
RRPtr<T>::~RRPtr(void) {
#ifdef DEBUG_PRINT
  cout << "~RRPtr:" << pointee_ << " #" << counter_ << endl;
  cout << "this:" << this << endl;
#endif
  if (!pointee_ || pointee_->isDying() ||
      (owner_ && owner_->isDying())) {
    pointee_ = nullptr;
    owner_ = nullptr;
    return;
  }

  if (owner_) {
    const bool owr = owner_->isRoot();
    const bool por = pointee_->isRoot();
    if (!owr) owner_->attach();
    if (!por) pointee_->attach();
    owner_->detachEdge(*pointee_);
    if (!por && !pointee_->isDying()) pointee_->detach();
    if (!owr && !owner_->isDying()) owner_->detach();
  } else {
    pointee_->detach();
  }

  pointee_ = nullptr;
  owner_ = nullptr;
}

template<class T>
void RRPtr<T>::setOwner(RRObject* owner) {
#ifdef DEBUG_PRINT
  cout << "RRPtr::setOwner() " << owner << " " << counter_ << endl;
#endif
  owner_ = owner;
}

template<class T>
RRPtr<T>& RRPtr<T>::operator=(const RRPtr& rhs) {
#ifdef DEBUG_PRINT
  cout << "RRPtr::operator=():" << counter_ <<
    " owner_: " << owner_ <<
    " pointee_: " << pointee_ <<
    " rhs.pointee_: " << rhs.pointee_ << endl;
#endif
  if (pointee_ == rhs.pointee_) return *this;

  T* old_pointee = pointee_;
  pointee_ = rhs.pointee_;
  if (pointee_) {
    if (owner_) {
      const bool owr = owner_->isRoot();
      const bool por = pointee_->isRoot();
      if (!owr) owner_->attach();
      if (!por) pointee_->attach();
      owner_->attachEdge(*pointee_);
      if (!por) pointee_->detach();
      if (!owr) owner_->detach();
    } else {
      pointee_->attach();
    }
  }
  if (old_pointee) {
    if (owner_) {
      const bool owr = owner_->isRoot();
      const bool por = old_pointee->isRoot();
      if (!owr) owner_->attach();
      if (!por) old_pointee->attach();
      owner_->detachEdge(*old_pointee);
      if (!por && !old_pointee->isDying()) old_pointee->detach();
      if (!owr && !owner_->isDying()) owner_->detach();
    } else {
      old_pointee->detach();
    }
  }

  return *this;
}

template<class T>
RRPtr<T>& RRPtr<T>::operator=(T* rhs) {
  return operator=(RRPtr<T>(rhs));
}

template<class T>
T* RRPtr<T>::operator->(void) const {
  return pointee_;
}

template<class T>
T& RRPtr<T>::operator*(void) const {
  return *pointee_;
}

template<class T>
RRPtr<T>::operator bool(void) const {
  return pointee_ != nullptr;
}

#endif // RRPtr_header
